package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Green_light_v2
import com.example.icefieldgame.ui.theme.Orange
import com.example.icefieldgame.ui.theme.Red
import com.example.icefieldgame.ui.theme.Yellow

@Composable
fun BetButton(modifier: Modifier = Modifier, gameOn: Boolean = false, cashAmount: Float,  onClick:() -> Unit = {}) {
    Box(modifier = modifier
        .background(if (!gameOn) Green_light_v2 else Red, RoundedCornerShape(3.dp))
        .clickable { onClick() }
        .clipToBounds(), contentAlignment = Alignment.Center) {
        Column(modifier = modifier.wrapContentSize(), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Text(if(!gameOn) stringResource(id = R.string.btn_bet) else stringResource(id = R.string.btn_cashout)
                , textAlign = TextAlign.Center
                , fontSize = 20.sp
                , fontWeight = FontWeight.Bold
                , color = if(!gameOn) Color.White else Color.Black)
            if(gameOn) {
                Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                    Text(String.format("%.2f", cashAmount), fontSize = 20.sp, color = Yellow, textAlign = TextAlign.Center)
                    Text(stringResource(id = R.string.currency), fontSize = 20.sp, color = Orange, textAlign = TextAlign.Center)
                }
            }
        }
    }
}