package com.example.icefieldgame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    NETWORK_ERROR_SCREEN("network_error"),
    WEB_VIEW("web_view"),
    MAIN_SCREEN("main")
}