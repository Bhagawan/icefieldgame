package com.example.icefieldgame.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.icefieldgame.R
import com.example.icefieldgame.data.static.UrlBack
import com.example.icefieldgame.ui.composables.BetButton
import com.example.icefieldgame.ui.composables.BetPanel
import com.example.icefieldgame.ui.composables.CoefficientCard
import com.example.icefieldgame.ui.composables.HistoryTable
import com.example.icefieldgame.ui.composables.LosePopup
import com.example.icefieldgame.ui.composables.RandomSizeButton
import com.example.icefieldgame.ui.composables.SizePanel
import com.example.icefieldgame.ui.composables.WinPopup
import com.example.icefieldgame.ui.theme.Green_light_v2
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Transparent_black
import com.example.icefieldgame.ui.view.IceFieldGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun MainScreen() {
    var initGame = remember { true }
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val balance by viewModel.balance.collectAsState()
    val gameOn by viewModel.gameOn.collectAsState()
    val bet by viewModel.bet.collectAsState()
    val totalCoefficient by viewModel.totalCoefficient.collectAsState()
    val nextCoefficient by viewModel.nextCoefficient.collectAsState()
    val history by viewModel.history.collectAsState()
    val size by viewModel.size.collectAsState()

    val winPopup by viewModel.winPopup.collectAsState()
    val losePopup by viewModel.lostPopup.collectAsState()

    val scrollState = rememberScrollState()

    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val height = maxHeight
        val width = maxWidth
        Column(modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState), horizontalAlignment = Alignment.CenterHorizontally) {
            Row(modifier = Modifier
                .width(width / 2)
                .height(30.dp)
                .padding(horizontal = 15.dp)
                .background(Transparent_black, RoundedCornerShape(3.dp))
                .padding(horizontal = 5.dp)) {
                Text(String.format("%.2f", balance), fontSize = 15.sp, color = Green_light_v2, textAlign = TextAlign.Start, modifier = Modifier.weight(1.0f, true))
                Text(stringResource(id = R.string.currency), fontSize = 15.sp, color = Green_light_v2, textAlign = TextAlign.End, modifier = Modifier.weight(1.0f, true))
            }
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .height(height * 0.7f - 30.dp)
                .background(Transparent_black), horizontalAlignment = Alignment.CenterHorizontally) {
                AndroidView(factory = { IceFieldGameView(it) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1.0f, true),
                    update = { view ->
                        if (initGame) {
                            view.setSize(size.first, size.second)
                            view.setInterface(object : IceFieldGameView.GameInterface {
                                override fun increaseCoefficient(next: Float) {
                                    viewModel.increaseCoefficient(next)
                                }

                                override fun fail() {
                                    viewModel.fail()
                                }

                                override fun win() {
                                    viewModel.win()
                                }

                            })
                            viewModel.gameOn.onEach { if(it) view.start() else view.stop() }.launchIn(viewModel.viewModelScope)
                            viewModel.size.onEach { view.setSize(it.first, it.second) }.launchIn(viewModel.viewModelScope)
                            initGame = false
                        }
                    })
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                    CoefficientCard(header = stringResource(id = R.string.header_next_win), modifier = Modifier.weight(1.0f, true), coefficient = nextCoefficient, total = nextCoefficient * totalCoefficient * bet)
                    CoefficientCard(header = stringResource(id = R.string.header_total_win), modifier = Modifier.weight(1.0f, true), coefficient = nextCoefficient, total = totalCoefficient * bet)
                }
            }
            Row(modifier = Modifier
                .height(height * 0.3f - 20.dp)
                .fillMaxWidth()
                .padding(10.dp)) {
                Column(modifier = Modifier
                    .weight(0.65f, true)
                    .fillMaxHeight()
                    .padding(end = 10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
                    BetPanel(amount = bet, modifier = Modifier.weight(1.0f, true), onIncrease = { viewModel.increaseBetAmount(10) }, onDecrease = { viewModel.increaseBetAmount(-10) })
                    Row(modifier = Modifier.weight(1.0f, true)) {
                        SizePanel(modifier = Modifier
                            .weight(0.65f, true)
                            .fillMaxHeight(),
                            size = size,
                            onDecrease = { viewModel.decreaseSize() },
                            onIncrease = { viewModel.increaseSize() })
                        RandomSizeButton(modifier = Modifier
                            .weight(0.35f, true)
                            .padding(start = 3.dp)
                            .fillMaxHeight(), onClick = { viewModel.randomSize() })
                    }
                }

                BetButton(
                    gameOn = gameOn,
                    cashAmount = bet * totalCoefficient,
                    modifier = Modifier
                        .weight(0.35f, true)
                        .fillMaxHeight(),
                    onClick = {
                        if(gameOn) viewModel.cashOut()
                        else viewModel.bet()
                    }
                )
            }

            Text(
                stringResource(id = R.string.history_header),
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .background(Transparent_black, RoundedCornerShape(10.dp))
                    .padding(10.dp))
            HistoryTable(history, modifier = Modifier.fillMaxWidth())
        }
    }
    winPopup?.let { WinPopup(amount = it, viewModel::hidePopup) }
    if(losePopup) LosePopup(viewModel::hidePopup)

}
