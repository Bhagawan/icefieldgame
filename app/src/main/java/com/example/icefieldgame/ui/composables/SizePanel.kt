package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Green_light_v2
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Transparent_white

@Composable
fun SizePanel(modifier: Modifier = Modifier, size: Pair<Int, Int>, onIncrease: () -> Unit = {}, onDecrease: () -> Unit) {
    BoxWithConstraints(modifier = modifier) {
        val height = maxHeight
        Row(modifier = Modifier
            .fillMaxSize()
            .border(width = 1.dp, color = Grey_light, shape = RoundedCornerShape(3.dp))
            .padding(5.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(stringResource(id = R.string.header_size), fontSize = 10.sp, color = Grey_light, textAlign = TextAlign.Start)
            Row(modifier = Modifier
                .padding(start = 5.dp)
                .weight(1.0f, true)
                .background(Transparent_white, RoundedCornerShape(3.dp))
                .padding(3.dp), verticalAlignment = Alignment.CenterVertically) {
                Icon(painterResource(id = R.drawable.round_chevron_left_24), contentDescription = null, tint = Grey_light, modifier = Modifier
                    .size(height * 0.3f)
                    .clickable(interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(bounded = false)) { onDecrease() })
                Text(size.first.toString(), fontSize = 15.sp, color = Green_light_v2, textAlign = TextAlign.End, modifier = Modifier.weight(1.0f, true))
                Text("X", fontSize = 15.sp, color = Grey_light, textAlign = TextAlign.Center)
                Text(size.second.toString(), fontSize = 15.sp, color = Green_light_v2, textAlign = TextAlign.Start, modifier = Modifier.weight(1.0f, true))
                Icon(painterResource(id = R.drawable.round_chevron_right_24), contentDescription = null, tint = Grey_light, modifier = Modifier
                    .size(height * 0.3f)
                    .clickable(interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(bounded = false)) { onIncrease() })
            }
        }
    }
}