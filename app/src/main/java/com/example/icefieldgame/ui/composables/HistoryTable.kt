package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.data.Bet
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Transparent_black

@Composable
fun HistoryTable(history: List<Bet>, modifier: Modifier = Modifier) {
    Column(modifier = modifier
        .padding(10.dp),
        verticalArrangement = Arrangement.spacedBy(3.dp),
        horizontalAlignment = Alignment.CenterHorizontally) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(Transparent_black, RoundedCornerShape(10.dp))
            .padding(horizontal = 10.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(stringResource(id = R.string.history_time),
                textAlign = TextAlign.Center,
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier.weight(1.0f, true))
            Text(stringResource(id = R.string.history_bet),
                textAlign = TextAlign.Center,
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier.weight(1.0f, true))
            Text(stringResource(id = R.string.history_coefficient),
                textAlign = TextAlign.Center,
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier.weight(1.0f, true))
            Text(stringResource(id = R.string.history_win),
                textAlign = TextAlign.Center,
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = Grey_light,
                modifier = Modifier.weight(1.0f, true))
        }
        history.sortedByDescending { it.time }
            .take(10)
            .forEach { HistoryItem(bet = it, modifier = Modifier.fillMaxWidth()) }
    }
}