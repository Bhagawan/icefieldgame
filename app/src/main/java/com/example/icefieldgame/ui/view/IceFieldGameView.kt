package com.example.icefieldgame.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.compose.ui.graphics.toArgb
import com.example.icefieldgame.data.static.Assets
import com.example.icefieldgame.ui.theme.Green_light
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Float.min
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class IceFieldGameView(context: Context): View(context){

    private var mWidth = 0
    private var mHeight = 0
    private var fieldWidth = 3
    private var fieldHeight = 2

    private var field = emptyList<List<IceFieldCell>>()
    private var coefficients = emptyList <Float>()
    private var yetiX = -1.0f
    private var yetiY = -1.0f

    private var cellSize = 1.0f

    private var activeColumn = 0

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_GAME = 1
    }

    private var currentState = STATE_PAUSE

    private var mInterface: GameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            cellSize = min(mWidth * 0.9f / fieldWidth.toFloat(), mHeight * 0.9f / fieldHeight.toFloat())
            generateField(fieldWidth, fieldHeight)
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawField(canvas)
        drawYeti(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE -> return true

            MotionEvent.ACTION_UP -> {
                if(currentState == STATE_GAME) click(event.x, event.y)
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun setSize(width: Int, height: Int) {
        if(currentState == STATE_PAUSE) {
            fieldWidth = width
            fieldHeight = height
            cellSize = min(mWidth * 0.9f / fieldWidth.toFloat(), mHeight * 0.9f / fieldHeight.toFloat())
            generateField(width, height)
            yetiX = -1.0f
            yetiY = -1.0f
        }
    }

    fun start() {
        if(currentState == STATE_PAUSE) startGame()
    }

    fun stop() {
        if(currentState == STATE_GAME) currentState = STATE_PAUSE
    }

    //// Private

    private fun drawField(c: Canvas) {
        field.forEach { row -> row.forEach { it.draw(c) } }

        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.textSize = 10.0f
        p.color = Green_light.toArgb()
        val coeffY = mHeight / 2.0f - (if (field.isNotEmpty()) field[0].size else 0 ) / 2.0f * cellSize - 15

        coefficients.forEachIndexed { index, coefficient ->
            val coeffX =( mWidth - field.size * cellSize + cellSize) /  2.0f + cellSize * index
            c.drawText(String.format("%.2f", coefficient), coeffX, coeffY, p)
        }
    }

    private fun drawYeti(c: Canvas) {
        if(yetiX > 0 && yetiY > 0) {
            Assets.yetiBitmap?.let {
                c.drawBitmap(it, null, Rect((yetiX - cellSize * 0.35f).toInt(),
                    (yetiY - cellSize * 0.35f).toInt(),
                    (yetiX + cellSize * 0.35f).toInt(),
                    (yetiY + cellSize * 0.35f).toInt()), Paint())
            }
        }
    }

    private fun generateField(width: Int, height: Int) {
        field = List(width) { x -> List(height) { y ->
            val cX = mWidth / 2.0f - (width / 2.0f - x - 0.5f) * cellSize
            val cY = mHeight / 2.0f - (height / 2.0f - y - 0.5f) * cellSize
            IceFieldCell(cX, cY, cellSize)
        } }
        coefficients = List(width) { 1.0f + (1.0f / height) * (it + 1) }
    }

    private fun startGame() {
        for(column in field.withIndex()) {
            for(tile in column.value) {
                tile.setGreyTint(column.index != 0)
                tile.setPurpleTint(false)
                tile.setCrack(false)
            }
        }
        yetiX = -1.0f
        yetiY = -1.0f
        activeColumn = 0
        currentState = STATE_GAME
    }

    private fun click(x: Float, y: Float) {
        if(field.size > activeColumn) {
            for(n in field[activeColumn].withIndex()) {
                if(n.value.clickInside(x, y)) {
                    val cracked = Random.nextInt(field[activeColumn].size)
                    yetiX = n.value.getX()
                    yetiY = n.value.getY()
                    field[activeColumn][cracked].setCrack(true)
                    if(cracked == n.index) {
                        mInterface?.fail()
                        currentState = STATE_PAUSE
                    } else if(activeColumn < field.size - 1) {
                        n.value.setPurpleTint(true)
                        activeColumn++
                        for(c in field[activeColumn]) c.setGreyTint(false)
                        mInterface?.increaseCoefficient(coefficients[activeColumn])
                    } else {
                        n.value.setPurpleTint(true)
                        currentState = STATE_PAUSE
                        mInterface?.win()
                    }
                }
            }
        }
    }

    interface GameInterface {
        fun increaseCoefficient(next: Float)
        fun fail()
        fun win()
    }
}