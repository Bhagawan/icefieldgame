package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Purple40

@Composable
fun RandomSizeButton(modifier: Modifier = Modifier, onClick: () -> Unit = {}) {
    Box(modifier = modifier
        .border(width = 2.dp, color = Purple40, RoundedCornerShape(5.dp)).clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = rememberRipple(bounded = false)
        ) {
            onClick()
        }, contentAlignment = Alignment.Center) {
        Text(stringResource(id = R.string.btn_random), fontSize = 10.sp, textAlign = TextAlign.Center, color = Grey_light)
    }
}