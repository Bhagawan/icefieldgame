package com.example.icefieldgame.ui.view

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.Rect
import androidx.compose.ui.graphics.toArgb
import com.example.icefieldgame.data.static.Assets
import com.example.icefieldgame.ui.theme.Purple
import com.example.icefieldgame.ui.theme.Transparent_dark
import kotlin.math.absoluteValue

class IceFieldCell(private val x: Float, private  val y: Float,private  val size: Float) {
    private val greyFilter = PorterDuffColorFilter(Transparent_dark.toArgb(), PorterDuff.Mode.DST_ATOP)
    private val purpleFilter = PorterDuffColorFilter(Purple.toArgb(), PorterDuff.Mode.SRC_ATOP)

    private var tintGrey = false
    private var tintPurple = false
    private var crack = false
    private val iceBitmap = Assets.iceBitmap ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val crackBitmap = Assets.crackBitmap ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)

    fun setGreyTint(active: Boolean) {
        tintGrey = active
    }

    fun setPurpleTint(active: Boolean) {
        tintPurple = active
    }

    fun setCrack(active: Boolean) {
        crack = active
    }

    fun clickInside(cX: Float, cY: Float) : Boolean = (cX - x).absoluteValue < size / 2.0f && (cY - y).absoluteValue < size / 2.0f

    fun getX() : Float = x
    fun getY() : Float = y

    fun draw(c: Canvas) {
        val p = Paint()
        if(tintGrey) p.colorFilter = greyFilter
        if(tintPurple) p.colorFilter = purpleFilter

        val r = size * 0.45f
        c.drawBitmap(iceBitmap, null, Rect((x - r).toInt(), (y - r).toInt(), (x + r).toInt(), (y + r).toInt()), p)
        if(crack) c.drawBitmap(crackBitmap, null, Rect((x - r).toInt(), (y - r).toInt(), (x + r).toInt(), (y + r).toInt()), p)
    }
}