package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Green_light
import com.example.icefieldgame.ui.theme.Purple_bright
import java.lang.Float.min

@Composable
fun WinPopup(amount: Float, onClose: () -> Unit) {
    val dencity = LocalDensity.current
    BoxWithConstraints(modifier = Modifier.fillMaxSize()
        .clickable( remember { MutableInteractionSource() }, null) { onClose() }, contentAlignment = Alignment.Center) {
        val w = with(dencity) { maxWidth.toPx() }
        val h = with(dencity) { maxHeight.toPx() }
        val brush = Brush.radialGradient(
            0.0f to Purple_bright ,
            1.0f to Color.Transparent,
            center = Offset(w / 2.0f, h / 2.0f),
            radius = min(w, h) * 0.45f
        )
        Column(modifier = Modifier.fillMaxSize().background(brush, RoundedCornerShape(30.0f)), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Text(stringResource(id = R.string.header_win), color = Color.White, fontSize = 30.sp, fontWeight =  FontWeight.Bold, textAlign = TextAlign.Center)
            Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                Text(String.format("%.2f", amount), color = Green_light, fontSize = 20.sp, fontWeight =  FontWeight.Bold, textAlign = TextAlign.Center)
                Text(stringResource(id = R.string.currency), color = Color.White, fontSize = 20.sp, textAlign = TextAlign.Center)
            }

        }
    }
}