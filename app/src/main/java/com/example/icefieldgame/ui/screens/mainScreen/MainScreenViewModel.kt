package com.example.icefieldgame.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import com.example.icefieldgame.data.Bet
import com.example.icefieldgame.data.static.CurrentAppData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.Date
import kotlin.random.Random

class MainScreenViewModel: ViewModel() {
    private val _balance = MutableStateFlow(CurrentAppData.balance)
    val balance = _balance.asStateFlow()

    private val _bet = MutableStateFlow(100)
    val bet = _bet.asStateFlow()

    private val _totalCoefficient = MutableStateFlow(1.0f)
    val totalCoefficient = _totalCoefficient.asStateFlow()

    private val _nextCoefficient = MutableStateFlow(1.01f)
    val nextCoefficient = _nextCoefficient.asStateFlow()

    private val _gameOn = MutableStateFlow(false)
    val gameOn = _gameOn.asStateFlow()

    private val _size = MutableStateFlow(3 to 2)
    val size = _size.asStateFlow()

    private val _history = MutableStateFlow(CurrentAppData.betsHistory.toList())
    val history = _history.asStateFlow()

    private val _winPopup = MutableStateFlow<Float?>(null)
    val winPopup = _winPopup.asStateFlow()

    private val _lostPopup = MutableStateFlow(false)
    val lostPopup = _lostPopup.asStateFlow()

    fun increaseBetAmount(amount: Int) {
        if(!gameOn.value) {
            _bet.tryEmit((bet.value + amount).coerceAtLeast(0).coerceAtMost(balance.value.toInt()))
        }
    }

    fun bet() {
        _gameOn.tryEmit(true)
        hidePopup()
        _balance.tryEmit(balance.value - bet.value)
        CurrentAppData.balance = balance.value
    }

    fun increaseCoefficient(next: Float) {
        _totalCoefficient.tryEmit(nextCoefficient.value)
        _nextCoefficient.tryEmit(next)
    }

    fun hidePopup() {
        _winPopup.tryEmit(null)
        _lostPopup.tryEmit(false)
    }

    fun win() {
        _totalCoefficient.tryEmit(nextCoefficient.value)
        CurrentAppData.betsHistory.add(Bet(Date(), bet.value, totalCoefficient.value, true))
        _history.tryEmit(CurrentAppData.betsHistory.toList())
        _balance.tryEmit(balance.value + bet.value * totalCoefficient.value)
        _winPopup.tryEmit(bet.value * totalCoefficient.value)
        _totalCoefficient.tryEmit(1.0f)
        _nextCoefficient.tryEmit(1 + 1.0f / size.value.second)
        _gameOn.tryEmit(false)
    }

    fun fail() {
        CurrentAppData.betsHistory.add(Bet(Date(), bet.value, totalCoefficient.value, false))
        _history.tryEmit(CurrentAppData.betsHistory.toList())
        _totalCoefficient.tryEmit(1.0f)
        _nextCoefficient.tryEmit(1 + 1.0f / size.value.second)
        _gameOn.tryEmit(false)
        _lostPopup.tryEmit(true)
    }

    fun increaseSize() {
        if(!gameOn.value) {
            _size.tryEmit((size.value.first + 1).coerceAtMost(6) to (size.value.second + 1).coerceAtMost(6))
        }
    }

    fun decreaseSize() {
        if(!gameOn.value) {
            _size.tryEmit((size.value.first - 1).coerceAtLeast(3) to (size.value.second - 1).coerceAtLeast(2))
        }
    }

    fun randomSize() {
        if(!gameOn.value) {
            _size.tryEmit( Random.nextInt(3,6) to Random.nextInt(2,6))
        }
    }

    fun cashOut() {
        CurrentAppData.betsHistory.add(Bet(Date(), bet.value, totalCoefficient.value, true))
        _history.tryEmit(CurrentAppData.betsHistory.toList())
        _balance.tryEmit(balance.value + bet.value * totalCoefficient.value)
        _winPopup.tryEmit(bet.value * totalCoefficient.value)
        _totalCoefficient.tryEmit(1.0f)
        _nextCoefficient.tryEmit(1 + 1.0f / size.value.second)
        _gameOn.tryEmit(false)
    }
}