package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Green_light_v2
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Transparent_white

@Composable
fun BetPanel(amount: Int, modifier: Modifier = Modifier, onIncrease: () -> Unit, onDecrease: () -> Unit) {
    BoxWithConstraints(modifier = modifier) {
        val height = maxHeight
        Row(modifier = Modifier
            .fillMaxSize()
            .border(width = 1.dp, color = Grey_light, shape = RoundedCornerShape(3.dp))
            .padding(5.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(stringResource(id = R.string.btn_bet), fontSize = 10.sp, color = Grey_light, textAlign = TextAlign.Start)
            Text(String.format("%.2f", amount.toFloat()), fontSize = 15.sp, color = Green_light_v2, textAlign = TextAlign.Start, modifier = Modifier
                .weight(1.0f, true)
                .padding(start = 10.dp))
            Text(stringResource(id = R.string.currency), fontSize = 15.sp, color = Grey_light, textAlign = TextAlign.Center, modifier = Modifier
                .weight(0.5f))

            Icon(painterResource(id = R.drawable.round_remove_24), contentDescription = null, tint = Grey_light, modifier = Modifier
                .padding(3.dp)
                .size(height* 0.5f)
                .aspectRatio(1.0f)
                .background(color = Transparent_white, CircleShape)
                .clickable { onDecrease() }
                .clipToBounds())

            Icon(painterResource(id = R.drawable.round_add_24), contentDescription = null, tint = Grey_light, modifier = Modifier
                .padding(3.dp)
                .size(height* 0.5f)
                .aspectRatio(1.0f)
                .background(color = Transparent_white, CircleShape)
                .clickable { onIncrease() }
                .clipToBounds())

        }
    }
}