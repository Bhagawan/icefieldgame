package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.data.Bet
import com.example.icefieldgame.ui.theme.Green_light_v2
import com.example.icefieldgame.ui.theme.Grey_light
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun HistoryItem(bet: Bet, modifier: Modifier = Modifier) {
    val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    val time = dateFormat.format(bet.time)
    val coeff = if(bet.win) String.format("%.2f", bet.coefficient) else "-"
    val win = if(bet.win) String.format("%.2f", bet.coefficient * bet.bet) else "-"
    val color = if(bet.win) Green_light_v2 else Grey_light
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        Text(
            time,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            bet.bet.toString(),
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            coeff,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
        Text(
            win,
            textAlign = TextAlign.Center,
            fontSize = 13.sp,
            fontWeight = FontWeight.Bold,
            color = color,
            modifier = Modifier.weight(1.0f, true))
    }
}