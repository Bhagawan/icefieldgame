package com.example.icefieldgame.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.icefieldgame.R
import com.example.icefieldgame.ui.theme.Grey_light
import com.example.icefieldgame.ui.theme.Transparent_white

@Composable
fun CoefficientCard(header: String, coefficient: Float, total: Float, modifier: Modifier = Modifier) {
    Column(modifier = modifier
        .background(Transparent_white, RoundedCornerShape(3.dp))
        .padding(horizontal = 10.dp, vertical = 3.dp), verticalArrangement = Arrangement.spacedBy(3.dp)) {
        Text(header, fontSize = 10.sp, color = Grey_light, textAlign = TextAlign.Start)
        Row(modifier = Modifier.fillMaxWidth().wrapContentHeight(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(3.dp)) {
            Text(String.format("%.2f", coefficient), fontSize = 13.sp, fontWeight = FontWeight.Bold, color = Color.White, textAlign = TextAlign.Start)
            Spacer(modifier = Modifier.weight(1.0f, true))
            Text(String.format("%.2f", total), fontSize = 13.sp, fontWeight = FontWeight.Bold, color = Color.White, textAlign = TextAlign.Start)
            Text(stringResource(id = R.string.currency), fontSize = 13.sp, color = Grey_light, textAlign = TextAlign.End)
        }
    }
}