package com.example.icefieldgame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Transparent_dark = Color(0x4D000000)
val Green_light = Color(0x4D5CFF2C)
val Green_light_v2 = Color(0xFF63B628)
val Transparent_black = Color(0x660E0E0E)
val Transparent_white = Color(0x33F5F5F5)
val Grey_light = Color(0xFFA2A0A0)
val Red = Color(0xFFC40808)
val Yellow = Color(0xFFEEBA00)
val Orange = Color(0xFFC56D21)
val Purple = Color(0x80C800FF)
val Purple_bright = Color(0xFF770097)