package com.example.icefieldgame.data

import androidx.annotation.Keep

@Keep
data class IceFieldSplashResponse(val url : String)