package com.example.icefieldgame.data.static

import android.content.Context
import android.graphics.Bitmap
import com.example.icefieldgame.data.AssetsState
import com.example.icefieldgame.util.api.ImageDownloader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlin.coroutines.EmptyCoroutineContext

object Assets {
    private val assetsDownloadScope = CoroutineScope(EmptyCoroutineContext)
    var iceBitmap: Bitmap? = null
    var crackBitmap: Bitmap? = null
    var yetiBitmap: Bitmap? = null

    private val _assetsState = MutableStateFlow(AssetsState.LOADING)
    val assetsState = _assetsState.asStateFlow()

    fun loadAssets(context: Context) {
        _assetsState.tryEmit(AssetsState.LOADING)
        ImageDownloader.urlToBitmap(context, assetsDownloadScope, UrlAssetIce, onSuccess = {
            iceBitmap = it
            checkCompletion()
        }, onError = {
            _assetsState.tryEmit(AssetsState.ERROR)
        })
        ImageDownloader.urlToBitmap(context, assetsDownloadScope, UrlAssetCrack, onSuccess = {
            crackBitmap = it
            checkCompletion()
        }, onError = {
            _assetsState.tryEmit(AssetsState.ERROR)
        })
        ImageDownloader.urlToBitmap(context, assetsDownloadScope, UrlAssetYeti, onSuccess = {
            yetiBitmap = it
            checkCompletion()
        }, onError = {
            _assetsState.tryEmit(AssetsState.ERROR)
        })
    }

    private fun checkCompletion() {
        if(iceBitmap != null &&
            crackBitmap != null &&
            yetiBitmap != null) _assetsState.tryEmit(AssetsState.LOADED)
    }

}