package com.example.icefieldgame.data.static

import com.example.icefieldgame.data.Bet

object CurrentAppData {
    var url = ""
    var balance = 1000.00f

    var betsHistory = ArrayList<Bet>()
}