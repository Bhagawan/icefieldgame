package com.example.icefieldgame.data.static

const val UrlSplash = "IceFieldGame/splash.php"
const val UrlLogo = "http://195.201.125.8/IceFieldGame/yeti.png"
const val UrlBack = "http://195.201.125.8/IceFieldGame/back.png"
const val UrlAssetIce = "http://195.201.125.8/IceFieldGame/ice_tile.png"
const val UrlAssetCrack = "http://195.201.125.8/IceFieldGame/crack.png"
const val UrlAssetYeti = "http://195.201.125.8/IceFieldGame/yeti.png"