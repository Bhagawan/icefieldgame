package com.example.icefieldgame.data

enum class AssetsState {
    LOADING, ERROR, LOADED
}