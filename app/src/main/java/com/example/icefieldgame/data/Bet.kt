package com.example.icefieldgame.data

import java.util.Date

data class Bet(val time: Date, val bet: Int, val coefficient: Float, val win: Boolean)
