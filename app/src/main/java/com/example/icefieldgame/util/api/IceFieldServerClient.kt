package com.example.icefieldgame.util.api

import com.example.icefieldgame.data.IceFieldSplashResponse
import com.example.icefieldgame.data.static.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface IceFieldServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<IceFieldSplashResponse>

    companion object {
        fun create() : IceFieldServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(IceFieldServerClient::class.java)
        }
    }
}