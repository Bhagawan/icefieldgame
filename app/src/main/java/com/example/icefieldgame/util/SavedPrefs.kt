package com.example.icefieldgame.util

import android.content.Context
import com.example.icefieldgame.data.Bet
import com.google.gson.Gson
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveTotalBalance(context: Context, balance: Float) = context.getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE).edit().putFloat("balance", balance).apply()

        fun getTotalBalance(context: Context): Float  = context.getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE).getFloat("balance", 1000.0f).run {
            if(this >= 100) this
            else 1000.0f
        }

        fun saveHistory(context: Context, history: List<Bet>) = context
            .getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE)
            .edit()
            .putString("history", Gson().toJson(history))
            .apply()

        fun getHistory(context: Context): List<Bet> = Gson()
            .fromJson(context.getSharedPreferences("IceFieldGame", Context.MODE_PRIVATE).getString("history", "[]"), Array<Bet>::class.java)
            .toList()
    }
}